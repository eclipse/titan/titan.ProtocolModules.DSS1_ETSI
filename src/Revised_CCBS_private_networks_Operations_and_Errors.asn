/*
///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:          Revised_CCBS_private_networks_Operations_and_Errors.asn
//  Description:   Functional protocol for ISDN
//
//  References:    ETSI EN 300 359-2 
//                 Integrated Services Digital Network (ISDN);
//                 Generic functional protocol for the support
//                 of supplementary services;
//  Rev:          R3C
//  Prodnr:       CNL 113 435
//  Updated:      2012.04.19.
//  Contact:      http://ttcn.ericsson.se
*/

Revised-CCBS-private-networks-Operations-and-Errors {itu-t identified-organization etsi (0) 359 private-networks-operations-and-errors (2)}

DEFINITIONS EXPLICIT TAGS ::=

 
BEGIN

 
  EXPORTS
      cCBS-T-Request,
      cCBS-T-Call,
      cCBS-T-Suspend,
      cCBS-T-Resume,
      cCBS-T-RemoteUserFree,
      cCBS-T-Available,
      longTermDenial,
      shortTermDenial;

 
  IMPORTS
      OPERATION,
      ERROR
    FROM Remote-Operations-Information-Objects
    {joint-iso-itu-t remote-operations (4) notation (0)} 

 
      notSubscribed
    FROM Revised-General-Errors
    {itu-t identified-organization etsi (0) 196 general-errors (2)}

 
      Address
    FROM Revised-Addressing-Data-Elements
    {itu-t identified-organization etsi (0) 196 addressing-data-elements (6)}

 
      Q931InformationElement
    FROM Revised-Embedded-Q931-Types
    {itu-t identified-organization etsi (0) 196 embedded-q931-types (7)};

  cCBS-T-Request OPERATION::=
    {
        ARGUMENT SEQUENCE {
          destinationAddress           Address,
          q931InfoElement              Q931InformationElement,
          retentionSupported           [1] IMPLICIT BOOLEAN DEFAULT FALSE,
          presentationAllowedIndicator [2] IMPLICIT BOOLEAN OPTIONAL,
          originatingAddress           Address OPTIONAL }
          -- contains HLC, LLC and BC information
          -- The use of presentationAllowedIndicator and originating-
          -- Address is specified in ..ETS 300 195.. for the interaction
          -- of CCBS and CLIP

          RESULT  BOOLEAN -- Default False
 
          ERRORS {
            shortTermDenial |
            notSubscribed |
            longTermDenial }   
            
          CODE global:{cCBS-T-OID 1}           
    }

  cCBS-T-Call OPERATION ::=
    {
      CODE global:{cCBS-T-OID 2}
    }  

 
  cCBS-T-Suspend OPERATION ::=
    {
      CODE global:{cCBS-T-OID 3}
    }

 
  cCBS-T-Resume OPERATION::=
    {
      CODE global:{cCBS-T-OID 4}
    }

 
  cCBS-T-RemoteUserFree OPERATION::=
    {
      CODE global:{cCBS-T-OID 5}
    }

 
  cCBS-T-Available OPERATION::=
    {
      CODE global:{cCBS-T-OID 6}
    }

 
  cCBS-T-OID OBJECT IDENTIFIER ::= {itu-t identified-organization etsi (0) 359 private-network-operations-and-errors (2)}

 
  longTermDenial   ERROR ::={CODE global:{cCBS-T-OID 20}}
  shortTermDenial  ERROR ::={CODE global:{cCBS-T-OID 21}}

 
END -- of CCBS-private-networks-operations-and-errors
